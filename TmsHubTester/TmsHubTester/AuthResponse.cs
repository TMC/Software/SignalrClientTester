﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmsHubTester
{

    public class AuthResponse
    {
        public String AccessToken { get; set; }
        public long Expiry { get; set; }
        public Extras Extras { get; set; }
    }

    public class Extras {

        public long OrgId { get; set; }
        public String OrgName { get; set; }
        public String UserDisplayName { get; set; }
    }

}
