﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmsHubTester
{
    class UrlConstants
    {
        public const String SIGNALR_URL = "https://signalr.telemedcare.com/api/v1";
        public const String AUTH_URL = "https://hydraauth.telemedcare.com/api/v1";
    }
}
