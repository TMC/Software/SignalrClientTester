﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmsHubTester
{
    class SignalrHelper
    {

        private String _accessToken;
        private long _orgId;


        /// <summary>
        /// Here we login using hydraauth.telemedcare.com url. 
        /// However, we can also use data.telemedcare.com to do the same thing
        /// The advantage of using hydraauth is that it is one step to get the access token
        /// However, you don't get an refreshToken when using hydraauth
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public async Task<bool> Login(String username, String password)
        {
            try
            {
                using (var client = new ApiClient())
                {
                    client.SetUrl(UrlConstants.AUTH_URL);
                    client.AddBasicHeader(username, password);
                    var result = await client.GetObjectAsync<AuthResponse>("auth/access/new");

                    _accessToken = result.AccessToken;
                    _orgId = result.Extras.OrgId;
                    return true;
                }
            }catch(Exception e)
            {
                return false;
            }

        }

        public async Task RegisterSocketConnection(String id)
        {
            var request = new RegisterConnectionRequest();
            request.ConnectionId = id;
            using(var client = new ApiClient())
            {
                client.SetUrl(UrlConstants.SIGNALR_URL);
                client.AddBearerHeader(_accessToken);
                var route = String.Format("org/{0}/socketconnection", _orgId);
                var result = await client.PostObjectAsync<RegisterConnectionRequest>(route, request);

            }
        }
    }
}
