﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmsHubTester
{
    /// <summary>
    /// Please be aware that this object will be serialised to camelCase inside the ApiClient PostObjectAsync method
    /// So when testing with Postman use property name like 'connectionId' instead of 'ConnectionId'
    /// </summary>
    class RegisterConnectionRequest
    {
        public String ConnectionId { get; set; }
    }
}
