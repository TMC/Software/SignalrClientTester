﻿using Microsoft.AspNet.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmsHubTester
{
    class Program
    {
        static void Main(string[] args)
        {

            Setup();

            Console.ReadKey();
        }

        private static async Task Setup()
        {
            Console.WriteLine("Enter username:");
            var username = Console.ReadLine();
            Console.WriteLine("Enter password:");
            var password = Console.ReadLine();

           

            var helper = new SignalrHelper();
            var loginSuccess = await helper.Login(username, password);
            if (loginSuccess)
            {
                Console.WriteLine("Please wait while we establish a socket connection...");
                //connect to signalr
             
                var hubConnection = new HubConnection(UrlConstants.SIGNALR_URL);

                IHubProxy hubProxy = hubConnection.CreateHubProxy("TmsHub");
                hubProxy.On("ping", PingHandler);
                hubProxy.On("measurementTaken", MeasurementTakenHandler);
                await hubConnection.Start();
                var connectionId = hubConnection.ConnectionId;
                Debug.WriteLine("ConnectionID: " + connectionId);

                //register the connectionid
                await helper.RegisterSocketConnection(connectionId);
                Console.WriteLine("Connection completed. You can login to apitestportal to simulate measurements and a log message will appear here.");
            }
            else
            {
                Console.WriteLine("Login failed. Press any key to exit");
            }
       

        }

        private static void PingHandler(object something)
        {
            Console.WriteLine("Ping received");
        }

        private static void MeasurementTakenHandler(object something)
        {
            Console.WriteLine("Measurement taken");
        }

    }
}
