﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace TmsHubTester
{
    public class ApiClient : HttpClient
    {
        private const String AUTH_HEADER = "Authorization";
        public ApiClient()
        {

        }

        public ApiClient(String url)
        {
            this.SetUrl(url);
        }

        public void SetUrl(String url)
        {
            if (url.Last() != '/')
            {
                url += '/';
            }
            this.BaseAddress = (new Uri(url));
        }

        public bool UrlIsValid(string url)
        {
            try
            {
                HttpWebRequest request = HttpWebRequest.Create(url) as HttpWebRequest;
                request.Timeout = 5000; //set the timeout to 5 seconds to keep the user from waiting too long for the page to load
                request.Method = "HEAD"; //Get only the header information -- no need to download any content

                HttpWebResponse response = request.GetResponse() as HttpWebResponse;

                int statusCode = (int)response.StatusCode;
                if (statusCode >= 100 && statusCode < 404) //Good requests
                {
                    return true;
                }
                else if (statusCode >= 500 && statusCode <= 510) //Server Errors
                {

                    return false;
                }
            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError) //400 errors
                {
                    return true;
                }
                else
                {

                }
            }
            catch (Exception ex)
            {

            }
            return false;
        }

        public virtual void AddBasicHeader(String username, String password)
        {
            var basicString = username + ":" + password;
            var base64string = Base64Encode(basicString);
            this.DefaultRequestHeaders.Remove(AUTH_HEADER);
            this.DefaultRequestHeaders.Add(AUTH_HEADER, "Basic " + base64string);
        }

        public void AddBearerHeader(String token)
        {
            AddHeader(AUTH_HEADER, "Bearer " + token);
        }

        public void AddHeader(String key, String value)
        {
            //tryaddwithoutvalidation allows us to add values like key=... which is required for Google FCM request
            this.DefaultRequestHeaders.TryAddWithoutValidation(key, value);
        }

        public string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }



        public async Task<T> GetObjectAsync<T>(String relativeApiPath)
        {
            var jsonString = "";
            HttpResponseMessage response;
            try
            {
                response = await GetAsync(relativeApiPath);
                jsonString = await response.Content.ReadAsStringAsync();
                if (response.IsSuccessStatusCode)
                {

                    var result = JsonConvert.DeserializeObject<T>(jsonString);
                    return result;
                }
                else
                {
                    var message = "Failed call to " + relativeApiPath + " ";
                    message += jsonString;
                    throw new ApplicationException(message);
                }

            }
            catch (ApplicationException ex)
            {
                throw;
            }
            catch (Exception e)
            {
                Debug.WriteLine("Failed call to " + relativeApiPath);
                Debug.WriteLine("JsonString: " + jsonString);
                var exceptionObject = JsonConvert.DeserializeObject<ExceptionResponse>(jsonString);
                if (exceptionObject == null)
                {
                    throw new Exception("Failed call to " + relativeApiPath);
                }
                else
                {
                    throw new Exception(exceptionObject.ExceptionMessage);
                }

            }

        }

        public async Task<T> ConvertResponse<T>(HttpResponseMessage message)
        {
            var jsonString = await message.Content.ReadAsStringAsync();
            try
            {

                var result = JsonConvert.DeserializeObject<T>(jsonString);
                return result;
            }
            catch (Exception e)
            {
                Debug.WriteLine("JsonString: " + jsonString);
                var exceptionObject = JsonConvert.DeserializeObject<ExceptionResponse>(jsonString);
                throw new Exception(exceptionObject.ExceptionMessage);
            }

        }

        /// <summary>
        /// Returns a HttpReponseMessage. Use ConvertResponse to get the response object.
        /// Will convert the object using camelCase property names.
        /// </summary>
        /// <typeparam name="T">The type of object you are posting</typeparam>
        /// <param name="url"></param>
        /// <param name="objectToPost">The object can use c# CapitalCaseProperties</param>
        /// <returns>HttpReponseMessage</returns>
        public async Task<HttpResponseMessage> PostObjectAsync<T>(String url, T objectToPost)
        {

            var json = JsonConvert.SerializeObject(objectToPost, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
            var stringContent = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await this.PostAsync(url, stringContent);
            if (!response.IsSuccessStatusCode)
            {
                var jsonString = await response.Content.ReadAsStringAsync();
                try
                {
                    var exceptionObject = JsonConvert.DeserializeObject<ExceptionResponse>(jsonString);
                    throw new ApplicationException(exceptionObject.ExceptionMessage);
                }
                catch
                {
                    throw new ApplicationException(jsonString);
                }

            }
            return response;

        }

        public async Task<HttpResponseMessage> PutObjectAsync<T>(String url, T o)
        {
            var json = JsonConvert.SerializeObject(o);
            var stringContent = new StringContent(json, Encoding.UTF8, "application/json");
            return await this.PutAsync(url, stringContent);
        }

        public void AcceptGzip()
        {
            this.DefaultRequestHeaders.AcceptEncoding.Add(new StringWithQualityHeaderValue("gzip"));
            this.DefaultRequestHeaders.AcceptEncoding.Add(new StringWithQualityHeaderValue("deflate"));
        }

        public void AcceptJson()
        {
            this.DefaultRequestHeaders
.Accept
.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
        }
    }


    public class ExceptionResponse
    {
        public string Message { get; set; }
        public string ExceptionMessage { get; set; }
        public string ExceptionType { get; set; }
        public string StackTrace { get; set; }
        public ExceptionResponse InnerException { get; set; }

        public override String ToString()
        {
            return "Message: " + Message + "\r\n "
                + "ExceptionMessage: " + ExceptionMessage + "\r\n "
                + "ExceptionType: " + ExceptionType + " \r\n "
                + "StackTrace: " + StackTrace + " \r\n ";
        }
    }
}
